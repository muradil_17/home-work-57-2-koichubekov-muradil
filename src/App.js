import React, {Component} from 'react';
import './App.css';
import Item from "./components/ItemUi/item";
import FinanceForm from "./components/financeForm/FinanceForm";
import Financeitem from "./components/FinanceItem/Financeitem";

class App extends Component {
  state = {
    value : '',
    valuePrice : '',
    list: [],
    id: 0,
    totalPrice : 0
  };

  change = event =>{
    this.setState({
      value: event.target.value
    });
  };

  price = event =>{
    this.setState({
      valuePrice: event.target.value
    })
  };

  totalPriceHide = ['totalpricehide'];
  add = ['add'];


  submit = ()=>{
    if (this.state.value === "" || this.state.valuePrice === ""){
      return false
    }else{
      this.totalPriceHide = ['totalPrice'];
      this.add = ['added'];

      this.list = [...this.state.list];
      this.listitem = {id: this.state.id + 1, value: this.state.value, valuePrice: parseInt(this.state.valuePrice)};
      this.totalPriceCopy = this.state.totalPrice + this.listitem.valuePrice;
      this.list.push(this.listitem);
      this.setState({value: '', valuePrice: '', list: this.list, id: this.state.id + 1, totalPrice: this.totalPriceCopy});
    }
  };

  remove = (id, valuePrice)=>{
    const list = [...this.state.list];
    const index = list.findIndex(task => task.id === id);
    list.splice(index,1);
    this.newTotalPriceCopy = this.state.totalPrice - valuePrice;
    this.setState({list: list, totalPrice: this.newTotalPriceCopy, id: this.state.id - 1});
    if(this.newTotalPriceCopy === 0){
      this.totalPriceHide = ['totalPriceHide'];
      this.add = ['add']
    }
  };

  render() {
    let  list = this.state.list.map((task)=>{
      return(
          <Item key={task.id}
                remove={()=> this.remove(task.id, task.valuePrice)}
                value={task.value}
                totalPrice={task.valuePrice}/>
      )
    });
    return (
        <div className="App">
          <FinanceForm change={event => this.change(event)}
                        price={event =>this.price(event)}
                        submit={() => this.submit()}
          />
          <Financeitem total={this.state.totalPrice}
                        class={this.totalPriceHide}
                        add={this.add}
          >{list}</Financeitem>
        </div>
    );
  }


}

export default App;
