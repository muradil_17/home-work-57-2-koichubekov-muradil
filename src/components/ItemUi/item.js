import React from 'react';

const Item = (props) => {
    return (
        <p key={props.id}>
            <span className="messageText">{props.value}</span>
            <span className="sumText">{props.totalPrice} <span className="course">KGS</span></span>
            <button onClick={props.remove}>X</button>
        </p>
    );
};

export default Item;