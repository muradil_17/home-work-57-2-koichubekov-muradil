import React from 'react';

const Financeitem = (props) => {
    return (
        <div className='financeitem'>
            {props.children}
            <span className={props.add}>Please add item name and his cost!</span>
            <div className={props.class}>
                <p className="total">Total spent:</p>
                <span>{props.total} KGS</span>
            </div>
        </div>
    );
};

export default Financeitem;