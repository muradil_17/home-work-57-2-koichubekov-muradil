import React from 'react';

const FinanceForm = (props) => {
    return (
        <div className="financeForm">
            <form action="#">
                <input type="text"
                       className="message"
                       title="Введите то, на что вы потратили деньги сегодня"
                        placeholder="Item name"
                        onChange={props.change}
                />
                <input type="number"
                        title="Summa"
                       className="Price"
                        placeholder="Total"
                        onChange={props.price}
                />
                <span>KGS</span>
                <button className='button' onClick={props.submit}>add</button>
            </form>
        </div>
    );
};

export default FinanceForm;